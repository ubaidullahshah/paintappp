import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class DrawingArea {
  Offset point;
  Paint areaPaint;

  DrawingArea({this.point, this.areaPaint});
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<DrawingArea> points = [];
  Color selectedColor;
  double strokeWidth;

  final List<Color> gradient = [
    Colors.green,
    Colors.blue,
    Colors.red,
    Colors.grey,
    Colors.purple,
    Colors.pink,
    Colors.amberAccent
  ];

  @override
  void initState() {
    super.initState();
    selectedColor = Colors.black;
    strokeWidth = 2.0;
  }

  showColorPicker(BuildContext context) {
    showDialog(context: context, builder: (context){
      return MaterialPicker(
        pickerColor: selectedColor,
        onColorChanged: (color) {
          this.setState(() {
            selectedColor = color;
            Navigator.pop(context);
          });
        },
      );
    });
  }

  onDrawStart(details){
    print("onPanDown");
    points.add(DrawingArea(
        point: details.localPosition,
        areaPaint: Paint()
          ..strokeCap = StrokeCap.round
          ..isAntiAlias = true
          ..color = selectedColor
          ..strokeWidth = strokeWidth));
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      Color.fromRGBO(0, 35, 135, 1.0),
                      Color.fromRGBO(0, 64, 87, 1.0),
                      Color.fromRGBO(0, 113, 33, 1.0),
                    ])),
          ),
          Center(
            child: MediaQuery.of(context).orientation == Orientation.landscape ?
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: width * 0.1,
                  height: height * 0.90,
                  decoration: BoxDecoration(
                    color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  ),
                  child: Center(
                    child: ListView(
                      shrinkWrap: true,
                      children: <Widget>[
                        PopupMenuButton(
                          itemBuilder: (context) => [
                            PopupMenuItem(
                              child: StatefulBuilder(
                                builder: (BuildContext context, StateSetter innerSetState){
                                  return Column(
                                    children: [
                                      Text("Thickness"),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: Slider(
                                              min: 1.0,
                                              max: 5.0,
                                              label: "Stroke $strokeWidth",
                                              activeColor: selectedColor,
                                              value: strokeWidth,
                                              onChanged: (double value) {
                                                innerSetState((){
                                                  strokeWidth = value;
                                                });
                                              },
                                            ),
                                          ),
                                          Text("${strokeWidth.toInt()}", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),),
                                        ],
                                      ),
                                      SizedBox(height: 10,),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        children: [
                                          GestureDetector(
                                            onTap: (){
                                              Navigator.pop(context);
                                              innerSetState(() {
                                                selectedColor = Colors.green;
                                              });
                                            },
                                            child: Container(
                                              height: 30,
                                              width: 30,
                                              decoration: BoxDecoration(
                                                color: Colors.green,
                                                borderRadius: BorderRadius.circular(15),
                                              ),
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: (){
                                              Navigator.pop(context);
                                              innerSetState(() {
                                                selectedColor = Colors.red;
                                              });
                                            },
                                            child: Container(
                                              height: 30,
                                              width: 30,
                                              decoration: BoxDecoration(
                                                color: Colors.red,
                                                borderRadius: BorderRadius.circular(15),
                                              ),
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: (){
                                              Navigator.pop(context);
                                              innerSetState(() {
                                                selectedColor = Colors.yellow;
                                              });
                                            },
                                            child: Container(
                                              height: 30,
                                              width: 30,
                                              decoration: BoxDecoration(
                                                color: Colors.yellow,
                                                borderRadius: BorderRadius.circular(15),
                                              ),
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: (){
                                              Navigator.pop(context);
                                              innerSetState(() {
                                                selectedColor = Colors.orange;
                                              });
                                            },
                                            child: Container(
                                              height: 30,
                                              width: 30,
                                              decoration: BoxDecoration(
                                                color: Colors.orange,
                                                borderRadius: BorderRadius.circular(15),
                                              ),
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: (){
                                              Navigator.pop(context);
                                              showColorPicker(context);
                                            },
                                            child: Container(
                                              height: 30,
                                              width: 30,
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(15),
                                                gradient: LinearGradient(
                                                  colors: gradient,
                                                  end: Alignment.bottomCenter,
                                                  begin: Alignment.topCenter,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  );
                                },
                              ),
                              value: 1,
                            ),
                          ],
                          icon: Icon(Icons.edit_outlined, color: selectedColor,),
                        ),
                        IconButton(
                            icon: Icon(
                              CupertinoIcons.clear_thick_circled,
                              color: Colors.black,
                            ),
                            onPressed: () {
                              this.setState((){
                                points.clear();
                              });
                            }),
                        IconButton(
                            icon: Icon(
                              CupertinoIcons.hand_point_left,
                              color: Colors.black,
                            ),
                            onPressed: () {

                            }),
                        IconButton(
                            icon: Icon(
                              CupertinoIcons.selection_pin_in_out,
                              color: Colors.black,
                            ),
                            onPressed: () {

                            }),
                        IconButton(
                            icon: Icon(
                              Icons.format_paint_outlined,
                              color: Colors.black,
                            ),
                            onPressed: () {

                            }),
                        IconButton(
                            icon: Icon(
                              CupertinoIcons.pencil_outline,
                              color: Colors.black,
                            ),
                            onPressed: () {}),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: width * 0.80,
                    height: height * 0.90,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(20.0)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.4),
                            blurRadius: 5.0,
                            spreadRadius: 1.0,
                          )
                        ]),
                    child: GestureDetector(
                      onPanDown: onDrawStart,
                      onPanUpdate:onDrawStart,
                      onPanEnd: (details) {
                        setState(() {
                          points.add(null);
                        });
                      },
                      child: SizedBox.expand(
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(20.0)),
                          child: CustomPaint(
                            painter: MyCustomPainter(points: points),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )
                :
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: width * 0.9,
                  height: height * 0.1,
                  decoration: BoxDecoration(
                    color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  ),
                  child: Center(
                    child: ListView(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      children: <Widget>[
                        PopupMenuButton(
                          itemBuilder: (context) => [
                            PopupMenuItem(
                              child: StatefulBuilder(
                                builder: (BuildContext context, StateSetter innerSetState){
                                  return Column(
                                    children: [
                                      Text("Thickness"),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: Slider(
                                              min: 1.0,
                                              max: 5.0,
                                              label: "Stroke $strokeWidth",
                                              activeColor: selectedColor,
                                              value: strokeWidth,
                                              onChanged: (double value) {
                                                innerSetState((){
                                                  strokeWidth = value;
                                                });
                                              },
                                            ),
                                          ),
                                          Text("${strokeWidth.toInt()}", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),),
                                        ],
                                      ),
                                      SizedBox(height: 10,),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        children: [
                                          GestureDetector(
                                            onTap: (){
                                              Navigator.pop(context);
                                              innerSetState(() {
                                                selectedColor = Colors.green;
                                              });
                                            },
                                            child: Container(
                                              height: 30,
                                              width: 30,
                                              decoration: BoxDecoration(
                                                color: Colors.green,
                                                borderRadius: BorderRadius.circular(15),
                                              ),
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: (){
                                              Navigator.pop(context);
                                              innerSetState(() {
                                                selectedColor = Colors.red;
                                              });
                                            },
                                            child: Container(
                                              height: 30,
                                              width: 30,
                                              decoration: BoxDecoration(
                                                color: Colors.red,
                                                borderRadius: BorderRadius.circular(15),
                                              ),
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: (){
                                              Navigator.pop(context);
                                              innerSetState(() {
                                                selectedColor = Colors.yellow;
                                              });
                                            },
                                            child: Container(
                                              height: 30,
                                              width: 30,
                                              decoration: BoxDecoration(
                                                color: Colors.yellow,
                                                borderRadius: BorderRadius.circular(15),
                                              ),
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: (){
                                              Navigator.pop(context);
                                              innerSetState(() {
                                                selectedColor = Colors.orange;
                                              });
                                            },
                                            child: Container(
                                              height: 30,
                                              width: 30,
                                              decoration: BoxDecoration(
                                                color: Colors.orange,
                                                borderRadius: BorderRadius.circular(15),
                                              ),
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: (){
                                              Navigator.pop(context);
                                              showColorPicker(context);
                                            },
                                            child: Container(
                                              height: 30,
                                              width: 30,
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(15),
                                                gradient: LinearGradient(
                                                  colors: gradient,
                                                  end: Alignment.bottomCenter,
                                                  begin: Alignment.topCenter,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  );
                                },
                              ),
                              value: 1,
                            ),
                          ],
                          icon: Icon(Icons.edit_outlined, color: selectedColor,),
                        ),
                        IconButton(
                            icon: Icon(
                              CupertinoIcons.clear_thick_circled,
                              color: Colors.black,
                            ),
                            onPressed: () {
                              this.setState((){
                                points.clear();
                              });
                            }),
                        IconButton(
                            icon: Icon(
                              CupertinoIcons.hand_point_left,
                              color: Colors.black,
                            ),
                            onPressed: () {

                            }),
                        IconButton(
                            icon: Icon(
                              CupertinoIcons.selection_pin_in_out,
                              color: Colors.black,
                            ),
                            onPressed: () {

                            }),
                        IconButton(
                            icon: Icon(
                              Icons.format_paint_outlined,
                              color: Colors.black,
                            ),
                            onPressed: () {

                            }),
                        IconButton(
                            icon: Icon(
                              CupertinoIcons.pencil_outline,
                              color: Colors.black,
                            ),
                            onPressed: () {}),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: width * 0.90,
                    height: height * 0.80,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(20.0)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.4),
                            blurRadius: 5.0,
                            spreadRadius: 1.0,
                          )
                        ]),
                    child: GestureDetector(
                      onPanDown: onDrawStart,
                      onPanUpdate: onDrawStart,
                      onPanEnd: (DragEndDetails details) {
                        Future.delayed(Duration(milliseconds: 400),(){
                          setState(() {
                            points.add(null);
                          });
                        });
                      },
                      child: SizedBox.expand(
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(20.0)),
                          child: CustomPaint(
                            painter: MyCustomPainter(points: points),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class MyCustomPainter extends CustomPainter {
  List<DrawingArea> points;

  MyCustomPainter({@required this.points});

  @override
  void paint(Canvas canvas, Size size) {
    Paint background = Paint()..color = Colors.white;
    Rect rect = Rect.fromLTWH(0, 0, size.width, size.height);
    canvas.drawRect(rect, background);
    canvas.clipRect(rect);

    for (int x = 0; x < points.length - 1; x++) {
      if (points[x] != null && points[x + 1] != null) {
        canvas.drawLine(points[x].point, points[x + 1].point, points[x].areaPaint);
      } else if (points[x] != null && points[x + 1] == null) {
        canvas.drawPoints(PointMode.points, [points[x].point], points[x].areaPaint);
      }
    }
  }

  @override
  bool shouldRepaint(MyCustomPainter oldDelegate) {
    return true;
  }
}